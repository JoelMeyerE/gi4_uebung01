#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>


int main(int argc, char* argv[], char* envp[])
{
pid_t pid =fork();
if(pid)
{
  printf("Elternprozess mit id %i\n", getpid());
  int pid_status;
  wait(&pid_status);
  if (WIFEXITED(pid_status))
  printf("Kindprozess wurde mit %i beendet\n", WEXITSTATUS(pid_status));
}

if(!pid)
{
  printf("Kindsprozess mit id %i\n", getpid());
  execv(argv[1],&argv[1]);
}


  return 0;
}
