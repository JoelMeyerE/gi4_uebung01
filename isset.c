#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[], char* envp[])
{
  /*Setze ein Int zum überprüfen ob '-v' eingegeben worden ist */
  int Vset=0;


  /* Überprüfe ob genau ein Argument gestzt worden ist*/

if(argc==1)
{
  puts("Kein Argument gesetzt");
  return 1;
}

if((strcmp(argv[1],"-v")==0))
{
  Vset=1;
  if(argc != 3)
  {
   puts("Mehr als ein Argument oder kein Argument nach '-v' eingegeben");
   return 1;
  }
}

else
{
 if(argc != 2)
 {
  puts("Mehr als ein Argument eingegeben");
  return 1;
}
}

  /* Rückgabe und Ausgaben */

 if(getenv(argv[1+Vset]) && Vset)
 {
  printf("%s=%s\n", argv[2], getenv(argv[2]));
  return 0;
 }
else if (getenv(argv[1]))
{
  printf("%s ist gesetzt\n", argv[1]);
  return 0;
}
else
{
  printf("%s ist nicht gesetzt\n", argv[1+Vset]);
  return 1;
}
}
